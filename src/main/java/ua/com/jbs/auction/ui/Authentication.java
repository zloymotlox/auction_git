package ua.com.jbs.auction.ui;

import ua.com.jbs.auction.businesslogic.UserOperations;
import ua.com.jbs.auction.entity.User;

import com.vaadin.Application;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;

/**
 * @author (astelit.ukr).
 */
public class Authentication extends CustomComponent {
  private TextField loginField;
  private PasswordField passwordField;
  private Button loginButton;
  private Button registrationButton;

  public Authentication() {
    createElements();
    addListeners();
  }
  
  private boolean validate(){
    try{
      loginField.validate();
      passwordField.validate();
      return true;
    }catch(InvalidValueException e){
      return false;
    }
  }
  
  private void addListeners(){
    registrationButton.addListener(new Button.ClickListener() {

      @Override
      public void buttonClick(ClickEvent event) {
        getApplication().getMainWindow().setContent(new Registration());
      }
    });
    
    loginButton.addListener(new Button.ClickListener() {
      private Application currentApp;
      @Override
      public void buttonClick(ClickEvent event) {
        if(!validate()){
          return;
        }
        UserOperations operations = new UserOperations();
        User user = operations.authenticate(loginField.getValue().toString(), passwordField.getValue().toString());
        if(user == null){
          getApplication().getMainWindow().showNotification("Wrong login or password!", 
                                                            Notification.TYPE_WARNING_MESSAGE);
        }else {
          currentApp = getApplication();
          AuctionUI aui = new AuctionUI(user);
          aui.addLogoutListener(new AuctionUI.LogoutListener() {
            @Override
            public void logout() {
              currentApp.close();
            }
          });
          getApplication().getMainWindow().setContent(aui);
        }
      }
    });
  }
  private void createElements(){
    setSizeFull();
    GridLayout rootLayout = new GridLayout();
    rootLayout.setSizeFull();
    setCompositionRoot(rootLayout);

    Panel root = new Panel("Authentication");
    rootLayout.addComponent(root);
    rootLayout.setComponentAlignment(root, Alignment.MIDDLE_CENTER);
    root.setSizeUndefined();

    FormLayout formLayout = new FormLayout();
    root.addComponent(formLayout);

    loginField = new TextField("Login");
    loginField.setRequired(true);
    formLayout.addComponent(loginField);

    passwordField = new PasswordField("Password");
    passwordField.setRequired(true);
    formLayout.addComponent(passwordField);

    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setSpacing(true);
    root.addComponent(buttonLayout);
    ((VerticalLayout)root.getLayout()).setComponentAlignment(buttonLayout, Alignment.TOP_CENTER);
    
    loginButton = new Button("Login");
    registrationButton = new Button("Registration");
    
    buttonLayout.addComponent(loginButton);
    buttonLayout.addComponent(registrationButton);
    
    buttonLayout.setComponentAlignment(loginButton, Alignment.TOP_LEFT);
    buttonLayout.setComponentAlignment(registrationButton, Alignment.TOP_RIGHT);

  }
}