package ua.com.jbs.auction.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ua.com.jbs.auction.businesslogic.BidOperations;
import ua.com.jbs.auction.entity.Bid;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;
import ua.com.jbs.auction.entitymanager.LotManager;
import ua.com.jbs.auction.entitymanager.UserManager;

import com.vaadin.Application;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;


/**
 *
 *
 * @author (astelit.ukr).
 */
public class BidsUI extends CustomComponent{
  public interface AddBidListener{
    void addBid(Bid bid);
  }
  
  private Set<AddBidListener> addBidListeners;
  
  private Lot activeLot;
  private User currentUser;
  private Table bidsTable;
  private Button newBidButton;
  
  public BidsUI(User user){
    this.addBidListeners = new HashSet<BidsUI.AddBidListener>();
    this.currentUser = user;
    createElements();
    addListeners();
  }
  
  public void addAddBidListener(AddBidListener listener){
    this.addBidListeners.add(listener);
  }
  
  public void removeAddBidListener(AddBidListener listener){
    this.addBidListeners.remove(listener);
  }
  
  private void addListeners(){
    newBidButton.addListener(new Button.ClickListener() {
      private ComponentContainer content;
      private Application currentApp;
      @Override
      public void buttonClick(ClickEvent event) {
        NewBidUI ui = new NewBidUI(currentUser, activeLot);
        content = BidsUI.this.getApplication().getMainWindow().getContent();
        currentApp = getApplication();
        ui.setCloseListener(new NewBidUI.CloseListener() {

          @Override
          public void close(Bid bid) {
            currentApp.getMainWindow().setContent(content);
            if(bid != null){
              showBids(activeLot);
              for(AddBidListener listener : addBidListeners){
                listener.addBid(bid);
              }
            }
          }
        });
        getApplication().getMainWindow().setContent(ui);
      }
    });
  }
  
  public void showBids(Lot lot){
    activeLot = lot;
    bidsTable.removeAllItems();
    List<Bid> bids = new BidOperations().getBids(lot);
    for (Bid bid : bids) {
      User bidder = new UserManager().findById(bid.getBidderId());
      bidsTable.addItem(new Object[] {
                            bid.getMoney() + " $",
                            new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date(bid.getDate()
                                .getTime())), 
                                bidder.getFirstName() + " " + bidder.getLastName()},
                        bid.getId());
    }
    if (lot.getOwnerId() != currentUser.getId() && lot.getState() == Lot.ACTIVE) {
      newBidButton.setEnabled(true);
    } else {
      newBidButton.setEnabled(false);
    }
  }
  
  private void createElements(){
    Panel bidsPanel = new Panel("Bids");
    bidsPanel.setSizeFull();
    setCompositionRoot(bidsPanel);

    VerticalLayout bidsPanelLayout = new VerticalLayout();
    bidsPanelLayout.setSizeFull();
    bidsPanelLayout.setMargin(true);
    bidsPanelLayout.setSpacing(true);
    bidsPanel.setLayout(bidsPanelLayout);

    bidsTable = new Table();
    bidsTable.setSizeFull();
    bidsTable.addContainerProperty("Bid", String.class, null);
    bidsTable.addContainerProperty("Date", String.class, null);
    bidsTable.addContainerProperty("Bidder", String.class, null);
    bidsPanelLayout.addComponent(bidsTable);
    bidsPanelLayout.setExpandRatio(bidsTable, 1.0f);

    newBidButton = new Button("New bid");
    newBidButton.setEnabled(false);
    bidsPanelLayout.addComponent(newBidButton);
    bidsPanelLayout.setComponentAlignment(newBidButton, Alignment.BOTTOM_RIGHT);
  }
}
