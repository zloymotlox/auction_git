package ua.com.jbs.auction.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import ua.com.jbs.auction.businesslogic.LotCanceller;
import ua.com.jbs.auction.businesslogic.LotOperations;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;

import com.vaadin.Application;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

/**
 * @author (astelit.ukr).
 */
public class LotsUI extends CustomComponent {
  public static interface LotSelectedListener {
    void lotSelected(Lot lot);
  }

  public static interface LotAddedListener {
    void lotAdded(Lot lot);
  }

  private Set<LotSelectedListener> lotSelectedListeners;
  private Set<LotAddedListener> lotAddedListeners;

  private Table lotsTable;
  private Button newLotButton;
  private Button refreshButton;
  private Long selectedLotId;
  private User currentUser;

  public LotsUI(User user) {
    BasicConfigurator.configure();
    this.currentUser = user;

    lotSelectedListeners = new HashSet<LotSelectedListener>();
    lotAddedListeners = new HashSet<LotAddedListener>();
    createElements();
    addListeners();
    showLots();
  }

  private void addListeners() {
    lotsTable.addListener(new Property.ValueChangeListener() {
      @Override
      public void valueChange(ValueChangeEvent event) {
        Logger.getLogger("Auction").info("value selected in lots table!");
        Long id = (Long) lotsTable.getValue();
        Lot lot = null;
        if (id != null) {
          selectedLotId = id;
          lot = new LotOperations().findById(id);
          for (LotSelectedListener listener : lotSelectedListeners) {
            listener.lotSelected(lot);
          }
        }
      }
    });

    newLotButton.addListener(new Button.ClickListener() {
      private ComponentContainer content;
      private Application currentApp;

      @Override
      public void buttonClick(ClickEvent event) {
        NewLotUI ui = new NewLotUI(currentUser);
        content = LotsUI.this.getApplication().getMainWindow().getContent();
        currentApp = getApplication();

        ui.setCloseListener(new NewLotUI.CloseListener() {
          @Override
          public void close(Lot addedLot) {
            if (addedLot != null) {
              for (LotAddedListener listener : lotAddedListeners) {
                listener.lotAdded(addedLot);
              }
            }

            currentApp.getMainWindow().setContent(content);
            showLots();
          }
        });

        getApplication().getMainWindow().setContent(ui);
      }
    });
    
    refreshButton.addListener(new Button.ClickListener() {
      
      @Override
      public void buttonClick(ClickEvent event) {
        showLots();
      }
    });
  }

  public void showLots() {
    lotsTable.removeAllItems();
    LotOperations lotOperations = new LotOperations();
    List<Lot> lots = lotOperations.getLots();
    for (Lot lot : lots) {
      lotsTable.addItem(new Object[] {lot.getId(), lot.getName(),
          new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date(lot.getFinishDate().getTime())),
          lotOperations.getState(lot)}, lot.getId());
    }
    lotsTable.select(selectedLotId);
  }

  private void createElements() {
    Panel lotsPanel = new Panel("lots");
    lotsPanel.setSizeFull();
    setCompositionRoot(lotsPanel);

    VerticalLayout lotsPanelLayout = new VerticalLayout();
    lotsPanelLayout.setSizeFull();
    lotsPanelLayout.setMargin(true);
    lotsPanelLayout.setSpacing(true);
    lotsPanel.setLayout(lotsPanelLayout);

    lotsTable = new Table();
    lotsTable.setImmediate(true);
    lotsTable.setSelectable(true);
    lotsTable.setMultiSelect(false);
    lotsTable.setSizeFull();
    lotsTable.addContainerProperty("Code", Long.class, null);
    lotsTable.addContainerProperty("Name", String.class, null);
    lotsTable.addContainerProperty("Finish date", String.class, null);
    lotsTable.addContainerProperty("State", String.class, null);
    lotsPanelLayout.addComponent(lotsTable);
    lotsPanelLayout.setExpandRatio(lotsTable, 1.0f);

    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setSpacing(true);
    lotsPanelLayout.addComponent(buttonLayout);
    lotsPanelLayout.setComponentAlignment(buttonLayout, Alignment.BOTTOM_RIGHT);
    
    refreshButton = new Button("Refresh table");
    buttonLayout.addComponent(refreshButton);
    
    newLotButton = new Button("New lot");
    buttonLayout.addComponent(newLotButton);
    
  }

  public void addlotSelectedListener(LotSelectedListener listener) {
    lotSelectedListeners.add(listener);
  }

  public void removelotSelectedListener(LotSelectedListener listener) {
    lotSelectedListeners.remove(listener);
  }

  public void addlotAddedListener(LotAddedListener listener) {
    lotAddedListeners.add(listener);
  }

  public void removelotAddedListener(LotAddedListener listener) {
    lotAddedListeners.remove(listener);
  }
}
