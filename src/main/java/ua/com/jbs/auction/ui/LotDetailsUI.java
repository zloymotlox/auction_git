package ua.com.jbs.auction.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ua.com.jbs.auction.businesslogic.LotOperations;
import ua.com.jbs.auction.businesslogic.UserOperations;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.Button.ClickEvent;

/**
 * @author (astelit.ukr).
 */
public class LotDetailsUI extends CustomComponent {
  
  public static interface CancelTradesListener{
    void cancelTrades(Lot lot);
  }
  
  private Set<CancelTradesListener> cancelTradesListeners;
  private Lot activeLot;
  private User currentUser;
  private Label lotCodeLabel;
  private Label lotNameLabel;
  private Label lotStateLabel;
  private Label lotFinishDateLabel;
  private Label lotOwnerLabel;
  private Label lotRemainingTimeLabel;
  private Label lotDescriptionLabel;
  private Label lotPriceLabel;
  private Button cancelTradesButton;

  public LotDetailsUI(User currentUser) {
    this.currentUser = currentUser;
    cancelTradesListeners = new HashSet<LotDetailsUI.CancelTradesListener>();
    createElements();
    addListeners();
  }
  
  public void redraw(){
    show(activeLot);
  }
  
  public void show(Lot lot){
    if(lot == null){
      return;
    }
    String remainingValue = "N/A";
    long dt = (lot.getFinishDate().getTime() - new Date().getTime()) / 60000; // minutes
    if(dt > 0 && lot.getState() == Lot.ACTIVE){
      long days = dt / (60 * 24);
      long hours = (dt - (60 * 24) * days) / 60;
      long minutes = dt - (60 * 24) * days - 60 * hours;
      remainingValue = days + " days " + hours + " hours " + minutes  + " minutes ";
    }
    
    LotOperations operations = new LotOperations();
    activeLot = lot;
    User owner = new UserOperations().findById(lot.getOwnerId());
    lotCodeLabel.setValue(String.valueOf(lot.getId()));
    lotNameLabel.setValue(lot.getName());
    lotStateLabel.setValue(operations.getState(lot));
    lotFinishDateLabel.setValue(new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date(lot.getFinishDate().getTime())));
    lotOwnerLabel.setValue(owner.getFirstName() + " " + owner.getLastName());
    lotRemainingTimeLabel.setValue(remainingValue);
    lotDescriptionLabel.setValue(lot.getDescription());
    lotPriceLabel.setValue(operations.getLotPrice(lot) + " $");
    if (owner.getId() == currentUser.getId() && lot.getState() == Lot.ACTIVE) {
      cancelTradesButton.setEnabled(true);
    } else {
      cancelTradesButton.setEnabled(false);
    }
  }
  
  public void addCancelTradesListener(CancelTradesListener listener){
    cancelTradesListeners.add(listener);
  }
  
  public void removeCancelTradesListener(CancelTradesListener listener){
    cancelTradesListeners.remove(listener);
  }
  
  private void addListeners(){
    cancelTradesButton.addListener(new Button.ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        LotOperations operations = new LotOperations();
        operations.cancelTrades(activeLot, currentUser);
        for(CancelTradesListener listener : cancelTradesListeners){
          listener.cancelTrades(activeLot);
        }
      }
    });
  }
  
  private void createElements(){
    Panel lotDetailsPanel = new Panel("Lot details");
    lotDetailsPanel.setWidth("100%");
    setCompositionRoot(lotDetailsPanel);

    HorizontalLayout lotDetailsPanelLayout = new HorizontalLayout();
    lotDetailsPanelLayout.setSpacing(true);
    lotDetailsPanelLayout.setMargin(true);
    lotDetailsPanelLayout.setSizeFull();
    lotDetailsPanel.setLayout(lotDetailsPanelLayout);

    GridLayout gridLayout = new GridLayout(2, 8);
    gridLayout.setSpacing(true);
    lotDetailsPanelLayout.addComponent(gridLayout);

    Label codeLabel = new Label("Code: ");
    codeLabel.setSizeUndefined();
    gridLayout.addComponent(codeLabel);
    gridLayout.setComponentAlignment(codeLabel, Alignment.MIDDLE_RIGHT);

    lotCodeLabel = new Label();
    codeLabel.setSizeUndefined();
    gridLayout.addComponent(lotCodeLabel);

    Label nameLabel = new Label("Name: ");
    nameLabel.setSizeUndefined();
    gridLayout.addComponent(nameLabel);
    gridLayout.setComponentAlignment(nameLabel, Alignment.MIDDLE_RIGHT);

    lotNameLabel = new Label();
    nameLabel.setSizeUndefined();
    gridLayout.addComponent(lotNameLabel);

    Label stateLabel = new Label("State: ");
    stateLabel.setSizeUndefined();
    gridLayout.addComponent(stateLabel);
    gridLayout.setComponentAlignment(stateLabel, Alignment.MIDDLE_RIGHT);

    lotStateLabel = new Label();
    stateLabel.setSizeUndefined();
    gridLayout.addComponent(lotStateLabel);

    Label finishDateLabel = new Label("Finish date: ");
    finishDateLabel.setSizeUndefined();
    gridLayout.addComponent(finishDateLabel);
    gridLayout.setComponentAlignment(finishDateLabel, Alignment.MIDDLE_RIGHT);

    lotFinishDateLabel = new Label();
    finishDateLabel.setSizeUndefined();
    gridLayout.addComponent(lotFinishDateLabel);

    Label ownerLabel = new Label("Owner: ");
    ownerLabel.setSizeUndefined();
    gridLayout.addComponent(ownerLabel);
    gridLayout.setComponentAlignment(ownerLabel, Alignment.MIDDLE_RIGHT);

    lotOwnerLabel = new Label();
    ownerLabel.setSizeUndefined();
    gridLayout.addComponent(lotOwnerLabel);

    Label remainingTimeLabel = new Label("Remaining time: ");
    remainingTimeLabel.setSizeUndefined();
    gridLayout.addComponent(remainingTimeLabel);
    gridLayout.setComponentAlignment(remainingTimeLabel, Alignment.MIDDLE_RIGHT);

    lotRemainingTimeLabel = new Label("!!! TO DO !!!");
    remainingTimeLabel.setSizeUndefined();
    gridLayout.addComponent(lotRemainingTimeLabel);

    Label descriptionLabel = new Label("Description: ");
    descriptionLabel.setSizeUndefined();
    gridLayout.addComponent(descriptionLabel);
    gridLayout.setComponentAlignment(descriptionLabel, Alignment.MIDDLE_RIGHT);

    lotDescriptionLabel = new Label();
    descriptionLabel.setSizeUndefined();
    gridLayout.addComponent(lotDescriptionLabel);

    Label priceLabel = new Label("Price: ");
    priceLabel.setSizeUndefined();
    gridLayout.addComponent(priceLabel);
    gridLayout.setComponentAlignment(priceLabel, Alignment.MIDDLE_RIGHT);

    lotPriceLabel = new Label();
    priceLabel.setSizeUndefined();
    gridLayout.addComponent(lotPriceLabel);

    cancelTradesButton = new Button("Cancel trades");
    cancelTradesButton.setEnabled(false);
    lotDetailsPanelLayout.addComponent(cancelTradesButton);
    lotDetailsPanelLayout.setComponentAlignment(cancelTradesButton, Alignment.BOTTOM_RIGHT);
  }
}
