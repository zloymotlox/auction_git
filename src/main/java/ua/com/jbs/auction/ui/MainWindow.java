package ua.com.jbs.auction.ui;

import com.vaadin.Application;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;

/**
 *
 *
 * @author (astelit.ukr).
 */
public class MainWindow extends Application{
  
  @Override
  public void init() {
    Window mainWindow = new Window();
    mainWindow.setContent(new Authentication());
    setMainWindow(mainWindow);
  }
}