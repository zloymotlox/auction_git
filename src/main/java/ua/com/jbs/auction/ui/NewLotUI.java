package ua.com.jbs.auction.ui;

import java.sql.Timestamp;
import java.util.Date;

import javax.xml.crypto.Data;

import ua.com.jbs.auction.businesslogic.LotOperations;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;
import ua.com.jbs.auction.ui.validation.DateFieldValidator;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

/**
 * @author (astelit.ukr).
 */
public class NewLotUI extends CustomComponent {

  private Button createButton;
  private Button cancelButton;
  private TextField lotNameField;
  private DateField lotFinishDateField;
  private TextField lotStartPriceField;
  private TextArea lotDescriptionField;
  private User user;
  private CloseListener closeListener;

  public static interface CloseListener {
    void close(Lot newLot);
  }

  public NewLotUI(final User user) {
    this.user = user;
    createElements();
    addListeners();
    
  }

  private void addListeners() {
    cancelButton.addListener(new Button.ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        if (closeListener != null) {
          closeListener.close(null);
        }
      }
    });

    createButton.addListener(new Button.ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        if(!validate()){
          return;
        }
        Date date = (Date) lotFinishDateField.getValue();
        Timestamp timestamp = new Timestamp(date.getTime());
        Lot addedLot = new LotOperations().createLot(user,
                                      lotNameField.getValue().toString(),
                                      timestamp,
                                      Float.parseFloat(lotStartPriceField.getValue().toString()),
                                      lotDescriptionField.getValue().toString());
        if (closeListener != null) {
          closeListener.close(addedLot);
        }
      }
    });
  }
  
  private boolean validate(){
    try{
      lotNameField.validate();
      lotFinishDateField.validate();
      lotStartPriceField.validate();
      lotDescriptionField.validate();
      int a = 0;
    }catch(InvalidValueException e){
      return false;
    }
    return true;
  }

  private void createElements() {
    setSizeFull();
    GridLayout rootLayout = new GridLayout();
    rootLayout.setSizeFull();
    setCompositionRoot(rootLayout);

    Panel root = new Panel("New lot");
    rootLayout.addComponent(root);
    rootLayout.setComponentAlignment(root, Alignment.MIDDLE_CENTER);
    root.setSizeUndefined();

    FormLayout formLayout = new FormLayout();
    root.addComponent(formLayout);

    lotNameField = new TextField("Lot name");
    lotNameField.setRequired(true);
    lotNameField.addValidator(new RegexpValidator("[A-Za-z ]{3,20}", "Enter correct lot name!"));
    formLayout.addComponent(lotNameField);

    lotFinishDateField = new DateField("Finish date");
    lotFinishDateField.setRequired(true);
    lotFinishDateField.addValidator(new DateFieldValidator("Enter correct date!"));
    lotFinishDateField.setResolution(DateField.RESOLUTION_MIN);
    formLayout.addComponent(lotFinishDateField);

    lotStartPriceField = new TextField("StartPrice");
    lotStartPriceField.setRequired(true);
    lotStartPriceField.addValidator(new RegexpValidator("[1-9][0-9]*(.[0-9]{1,2})?", "Enter correct price!"));
    formLayout.addComponent(lotStartPriceField);

    lotDescriptionField = new TextArea("Description");
    lotDescriptionField.setRequired(true);
    formLayout.addComponent(lotDescriptionField);

    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setSpacing(true);
    root.addComponent(buttonLayout);
    ((VerticalLayout) root.getLayout()).setComponentAlignment(buttonLayout, Alignment.TOP_CENTER);

    createButton = new Button("Create");
    cancelButton = new Button("Cancel");

    buttonLayout.addComponent(createButton);
    buttonLayout.addComponent(cancelButton);

    buttonLayout.setComponentAlignment(createButton, Alignment.TOP_LEFT);
    buttonLayout.setComponentAlignment(cancelButton, Alignment.TOP_RIGHT);
  }

  public void setCloseListener(CloseListener listener) {
    this.closeListener = listener;
  }
}
