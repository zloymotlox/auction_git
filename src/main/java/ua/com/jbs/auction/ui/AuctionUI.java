package ua.com.jbs.auction.ui;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import ua.com.jbs.auction.businesslogic.LotCanceller;
import ua.com.jbs.auction.entity.Bid;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;

/**
 * @author (astelit.ukr).
 */
public class AuctionUI extends CustomComponent {
  public static interface LogoutListener {
    void logout();
  }
  private Set<LogoutListener> logoutListeners = new HashSet<AuctionUI.LogoutListener>();
  
  private User currentUser;
  private Button logoutButton;
  private LotsUI lotsUI;
  private LotDetailsUI lotDetailsUI;
  private BidsUI bidsUI;

  public AuctionUI(final User user) {
    Logger.getLogger("Auction").info("User " + user.getFirstName() + " " + user.getLastName()
                                     + " Authenticated");
    this.currentUser = user;
    createElements();
    addListeners();
  }
  
  public void addLogoutListener(LogoutListener listener){
    logoutListeners.add(listener);
  }
  
  public void removeLogoutListener(LogoutListener listener){
    logoutListeners.remove(listener);
  }

  private void addListeners() {
    logoutButton.addListener(new Button.ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        Logger.getLogger("Auction").info("User " + currentUser.getFirstName() + " " + currentUser.getLastName() + " logged out!");
        for(LogoutListener listener: logoutListeners){
          listener.logout();
        }
      }
    });

    lotsUI.addlotSelectedListener(new LotsUI.LotSelectedListener() {
      @Override
      public void lotSelected(Lot lot) {
        lotDetailsUI.show(lot);
        bidsUI.showBids(lot);
      }
    });

    lotsUI.addlotAddedListener(new LotsUI.LotAddedListener() {

      @Override
      public void lotAdded(Lot lot) {
        Logger.getLogger("Auction").info("User " + currentUser.getFirstName() + " " + currentUser.getLastName() + " added new lot!");
        try {
          JobDetail job = new JobDetail();
          job.setName("LotCanceller");
          job.setJobClass(LotCanceller.class);
          job.getJobDataMap().put("LotId", lot.getId());

          CronTrigger trigger = new CronTrigger();
          trigger.setName("LotCanceller");
          Timestamp finishDate = lot.getFinishDate();
          Logger.getLogger("Auction").info("job added: 0 " + finishDate.getMinutes() + " " + finishDate.getHours() 
                                           + " " + finishDate.getDate() + " " + (finishDate.getMonth() + 1) + " ? " 
                                           + (finishDate.getYear() + 1900));
          
          trigger.setCronExpression("0 " + finishDate.getMinutes() + " " + finishDate.getHours() + " " 
                                    + finishDate.getDate() + " " + (finishDate.getMonth() + 1) + " ? "  
                                    + (finishDate.getYear() + 1900));

          // schedule it
          Scheduler scheduler = new StdSchedulerFactory().getScheduler();
          scheduler.start();
          scheduler.scheduleJob(job, trigger);
        } catch (ParseException e) {
          Logger.getLogger("Auction").error(e.getMessage());
        } catch(SchedulerException e){
          Logger.getLogger("Auction").error(e.getMessage());
        }
      }
    });

    bidsUI.addAddBidListener(new BidsUI.AddBidListener() {
      @Override
      public void addBid(Bid bid) {
        if (bid != null) {
          lotDetailsUI.redraw();
          getApplication().getMainWindow().showNotification("New bid was added!",
                                                            Notification.TYPE_HUMANIZED_MESSAGE);
          Logger.getLogger("Auction").info("User " + currentUser.getFirstName() + " " + currentUser.getLastName() + " added new bid!");
        }
      }
    });

    lotDetailsUI.addCancelTradesListener(new LotDetailsUI.CancelTradesListener() {
      @Override
      public void cancelTrades(Lot lot) {
        lotsUI.showLots();
        getApplication().getMainWindow().showNotification("Trades were cancelled!",
                                                          Notification.TYPE_HUMANIZED_MESSAGE);
      }
    });
  }

  private void createElements() {
    // root vertical layout
    VerticalLayout rootLayout = new VerticalLayout();
    rootLayout.setSizeFull();
    rootLayout.setMargin(true);
    setCompositionRoot(rootLayout);

    // header horisontal layout
    HorizontalLayout headerLayout = new HorizontalLayout();
    headerLayout.setWidth("100%");
    rootLayout.addComponent(headerLayout);

    Label auctionLabel = new Label("<h2>Auction</h2>");
    auctionLabel.setContentMode(Label.CONTENT_XHTML);
    headerLayout.addComponent(auctionLabel);

    HorizontalLayout userLayout = new HorizontalLayout();
    userLayout.setSpacing(true);
    userLayout.setSizeUndefined();
    headerLayout.addComponent(userLayout);
    headerLayout.setComponentAlignment(userLayout, Alignment.TOP_RIGHT);

    Label userLabel = new Label("User: " + currentUser.getFirstName() + " "
        + currentUser.getLastName());
    // userLabel.setSizeUndefined();
    userLayout.addComponent(userLabel);
    userLayout.setComponentAlignment(userLabel, Alignment.MIDDLE_RIGHT);

    logoutButton = new Button("Logout");
    userLayout.addComponent(logoutButton);
    userLayout.setComponentAlignment(logoutButton, Alignment.MIDDLE_RIGHT);

    // main horizontal layout
    HorizontalLayout mainLayout = new HorizontalLayout();
    mainLayout.setSpacing(true);
    mainLayout.setSizeFull();
    setHeight("100%");
    rootLayout.addComponent(mainLayout);
    rootLayout.setExpandRatio(mainLayout, 1.0f);

    // Left column
    lotsUI = new LotsUI(currentUser);
    lotsUI.setSizeFull();
    mainLayout.addComponent(lotsUI);

    // right column

    VerticalLayout rightColumnLayout = new VerticalLayout();
    rightColumnLayout.setSizeFull();
    mainLayout.addComponent(rightColumnLayout);

    // lot details panel
    lotDetailsUI = new LotDetailsUI(currentUser);
    lotDetailsUI.setWidth("100%");
    rightColumnLayout.addComponent(lotDetailsUI);

    // bids panel
    bidsUI = new BidsUI(currentUser);
    bidsUI.setSizeFull();
    rightColumnLayout.addComponent(bidsUI);
    rightColumnLayout.setExpandRatio(bidsUI, 1.0f);
  }
}