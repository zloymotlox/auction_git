package ua.com.jbs.auction.ui.validation;

import java.util.Date;

import com.vaadin.data.Validator;

/**
 *
 *
 * @author (astelit.ukr).
 */
public class DateFieldValidator implements Validator{
  private String message;
  
  public DateFieldValidator(String message) {
    this.message = message;
  }

  @Override
  public void validate(Object value) throws InvalidValueException {
    if(!isValid(value)) {
      throw new InvalidValueException(message);
    }
  }

  @Override
  public boolean isValid(Object value) {
    Date date = (Date)value;
    if(date == null){
      return false;
    }
    if(date.getTime() <= new Date().getTime()){
      return false;
    }
    return true;
  }

}
