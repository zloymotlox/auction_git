package ua.com.jbs.auction.ui;

import ua.com.jbs.auction.businesslogic.UserOperations;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;

/**
 *
 *
 * @author (astelit.ukr).
 */
public class Registration extends CustomComponent{
  private TextField loginField;
  private PasswordField passwordField;
  private TextField firstNameField;
  private TextField lastNameField;
  private Button cancelButton;
  private Button registerButton;
  
  
  public Registration(){
    createElements();
    addListeners();
  }
  
  private boolean validate(){
    try{
      loginField.validate();
      passwordField.validate();
      firstNameField.validate();
      lastNameField.validate();
      return true;
    }catch(InvalidValueException e){
      return false;
    }
  }
  
  private void addListeners(){
    cancelButton.addListener(new Button.ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        getApplication().getMainWindow().setContent(new Authentication());
        
      }
    });
    
    registerButton.addListener(new Button.ClickListener() {
      @Override
      public void buttonClick(ClickEvent event) {
        if(!validate()){
          return;
        }
        UserOperations operatins = new UserOperations();
        if(operatins.registrateUser(loginField.getValue().toString(), 
                                    passwordField.getValue().toString(), 
                                    firstNameField.getValue().toString(), 
                                    lastNameField.getValue().toString()) != null){
          getApplication().getMainWindow().setContent(new Authentication());
        }else{
          getApplication().getMainWindow().showNotification("User " + loginField.getValue().toString() + 
                                                            " is already in use!", Notification.TYPE_WARNING_MESSAGE);
        }
      }
    });
  }
  
  private void createElements(){
    setSizeFull();
    GridLayout rootLayout = new GridLayout();
    rootLayout.setSizeFull();
    setCompositionRoot(rootLayout);
    
    
    Panel root = new Panel("Registration");
    rootLayout.addComponent(root);
    rootLayout.setComponentAlignment(root, Alignment.MIDDLE_CENTER);
    root.setSizeUndefined();
    
    FormLayout formLayout = new FormLayout();
    root.addComponent(formLayout);
    
    loginField = new TextField("Login");
    loginField.setRequired(true);
    loginField.addValidator(new RegexpValidator("[A-Za-z_]+", "Login can contain only letters and '_'"));
    formLayout.addComponent(loginField);
    
    passwordField = new PasswordField("Password");
    passwordField.setRequired(true);
    formLayout.addComponent(passwordField);
    
    firstNameField = new TextField("First name");
    firstNameField.addValidator(new RegexpValidator("[A-Za-z]+", "First name cat contain only letters"));
    firstNameField.setRequired(true);
    formLayout.addComponent(firstNameField);
    
    lastNameField = new TextField("Last name");
    lastNameField.addValidator(new RegexpValidator("[A-Za-z]+", "Last name cat contain only letters"));
    lastNameField.setRequired(true);
    formLayout.addComponent(lastNameField);
    
    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setSpacing(true);
    root.addComponent(buttonLayout);
    ((VerticalLayout)root.getLayout()).setComponentAlignment(buttonLayout, Alignment.TOP_CENTER);
    
    registerButton = new Button("Register");
    cancelButton = new Button("Cancel");
    
    buttonLayout.addComponent(registerButton);
    buttonLayout.addComponent(cancelButton);
    
    buttonLayout.setComponentAlignment(registerButton, Alignment.TOP_LEFT);
    buttonLayout.setComponentAlignment(cancelButton, Alignment.TOP_RIGHT);
  }
}