package ua.com.jbs.auction.ui;

import ua.com.jbs.auction.businesslogic.BidOperations;
import ua.com.jbs.auction.businesslogic.LotOperations;
import ua.com.jbs.auction.entity.Bid;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;

import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.validator.DoubleValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;

/**
 * @author (astelit.ukr).
 */
public class NewBidUI extends CustomComponent {
  
  public static interface CloseListener{
    void close(Bid addedBid);
  }
  private CloseListener closeListener;
  
  public NewBidUI(final User user, final Lot lot){
    setSizeFull();
    GridLayout rootLayout = new GridLayout();
    rootLayout.setSizeFull();
    setCompositionRoot(rootLayout);
    
    Panel root = new Panel("New Bid");
    rootLayout.addComponent(root);
    rootLayout.setComponentAlignment(root, Alignment.MIDDLE_CENTER);
    root.setSizeUndefined();
    
    HorizontalLayout layout = new HorizontalLayout();
    layout.setSpacing(true);
    root.addComponent(layout);
    
    Label bidLabel = new Label("Bid:");
    layout.addComponent(bidLabel);
    layout.setComponentAlignment(bidLabel, Alignment.MIDDLE_LEFT);
    
    final TextField moneyField = new TextField();
    moneyField.addValidator(new DoubleValidator("invalid bid!"));
    layout.addComponent(moneyField);
    
    Label usdLabel = new Label("$");
    layout.addComponent(usdLabel);
    layout.setComponentAlignment(usdLabel, Alignment.MIDDLE_LEFT);
    
    Button okButton = new Button("Ok");
    layout.addComponent(okButton);
    
    okButton.addListener(new Button.ClickListener() {
      
      @Override
      public void buttonClick(ClickEvent event) {
        try{
          moneyField.validate();
        }catch(InvalidValueException e){
          getApplication().getMainWindow().showNotification("Enter correct data!", Notification.TYPE_WARNING_MESSAGE);
          return;
        }
        float money  = Float.parseFloat(moneyField.getValue().toString());
        Bid bid = new BidOperations().createBid(user, lot, money);
        //getApplication().getMainWindow().setContent(parentComponent);
        if(closeListener != null){
          closeListener.close(bid);
        }
      }
    });
  }
  
  public void setCloseListener(CloseListener listener){
    this.closeListener = listener;
  }
}