package ua.com.jbs.auction.entitymanager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import ua.com.jbs.auction.entity.User;
/**
 *
 *
 * @author (astelit.ukr).
 */
public class UserManager {
  private static final String IS_USER_EXIST = "SELECT u.login FROM User u WHERE u.login = :login";
  private static final String GET_USER_BY_LOGIN_PASSWORD = "SELECT u FROM User u WHERE u.login =:login AND u.password =:password";
  private static final String FIND_BY_ID = "SELECT u FROM User u WHERE u.id =:id";
  EntityManagerFactory emf =
      Persistence.createEntityManagerFactory("mysql");
  
  public void persist(User user){
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(user);
      em.getTransaction().commit();
    } catch (Exception e) {
      em.getTransaction().rollback();
    }
  }
  
  public User findById(long id){
    EntityManager em = emf.createEntityManager();
    Query query = em.createQuery(FIND_BY_ID);
    query.setParameter("id", id);
    List<User> result = query.getResultList();
    if(result.isEmpty()){
      return null;
    }
    return result.get(0);
  }
  
  public boolean isUserExist(String login){
    EntityManager em = emf.createEntityManager();
    Query query = em.createQuery(IS_USER_EXIST);
    query.setParameter("login", login);
    List<Object> result = query.getResultList();
    return !result.isEmpty();
  }
  
  public User getUserByLoginPasword(String login, String password){
    EntityManager em = emf.createEntityManager();
    Query query = em.createQuery(GET_USER_BY_LOGIN_PASSWORD);
    query.setParameter("login", login);
    query.setParameter("password", password);
    List<User> result = query.getResultList();
    if(result.isEmpty()){
      return null;
    }
    return result.get(0);
  }
}