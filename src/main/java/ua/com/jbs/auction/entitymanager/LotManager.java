package ua.com.jbs.auction.entitymanager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;

/**
 * @author (astelit.ukr).
 */
public class LotManager {

  private static final String GET_LOTS = "SELECT l FROM Lot l";
  private static final String FIND_BY_ID = "SELECT l FROM Lot l WHERE l.id =:id";
  EntityManagerFactory emf = Persistence.createEntityManagerFactory("mysql");

  public Lot persist(Lot lot) {
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(lot);
      em.getTransaction().commit();
      return lot;
    } catch (Exception e) {
      em.getTransaction().rollback();
      return null;
    }
  }

  public void update(Lot lot) {
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.merge(lot);
      em.getTransaction().commit();
    } catch (Exception e) {
      em.getTransaction().rollback();
    }
  }

  public List<Lot> getLots() {
    EntityManager em = emf.createEntityManager();
    Query query = em.createQuery(GET_LOTS);
    return query.getResultList();
  }

  public Lot findById(long id) {
    EntityManager em = emf.createEntityManager();
    Query query = em.createQuery(FIND_BY_ID);
    query.setParameter("id", id);
    List<Lot> result = query.getResultList();
    if (result.isEmpty()) {
      return null;
    } else {
      return result.get(0);
    }
  }
}
