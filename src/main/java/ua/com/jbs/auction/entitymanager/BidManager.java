package ua.com.jbs.auction.entitymanager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import ua.com.jbs.auction.entity.Bid;
import ua.com.jbs.auction.entity.Lot;

/**
 *
 *
 * @author (astelit.ukr).
 */
public class BidManager {
  private static final String FIND_BY_LOT_ID = "SELECT b FROM Bid b WHERE b.lotId =:lotid";
  private static final String GET_MAX_BID = "SELECT b FROM Bid b WHERE b.lotId =:lotid ORDER BY b.money DESC";
  
  EntityManagerFactory emf =
      Persistence.createEntityManagerFactory("mysql");
  
  public void persist(Bid bid){
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(bid);
      em.getTransaction().commit();
    } catch (Exception e) {
      em.getTransaction().rollback();
    }
  }
  
  public List<Bid> findByLotId(long lotId){
    EntityManager em = emf.createEntityManager();
    Query query = em.createQuery(FIND_BY_LOT_ID);
    query.setParameter("lotid", lotId);
    return query.getResultList();
  }
  
  public Bid getMaxBid(long lotId){
    EntityManager em = emf.createEntityManager();
    Query query = em.createQuery(GET_MAX_BID);
    query.setParameter("lotid", lotId);
    query.setMaxResults(1);
    List<Bid> result =  query.getResultList();
    if(result.isEmpty()){
      return null;
    }
    return result.get(0);
  }
}