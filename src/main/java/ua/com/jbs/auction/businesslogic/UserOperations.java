package ua.com.jbs.auction.businesslogic;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ua.com.jbs.auction.entity.User;
import ua.com.jbs.auction.entitymanager.UserManager;

/**
 * @author (astelit.ukr).
 */
public class UserOperations {

  public User registrateUser(String login, String password, String firstName, String lastName){
    password = md5(password);
    User user = null;
    UserManager userManager = new UserManager();
    if(!userManager.isUserExist(login)){
      user = new User(login, password, firstName, lastName);
      userManager.persist(user);
    }
    return user;
  }
  
  public User authenticate(String login,  String password){
    password = md5(password);
    UserManager userManager = new UserManager();
    return userManager.getUserByLoginPasword(login, password);
  }
  
  private String md5(String password){
    byte [] passBytes = password.getBytes();
    try{
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      password = new String(md5.digest(passBytes));
    }catch(NoSuchAlgorithmException e){
      
    }
    return password;
  }
  
  public User findById(long id){
    return new UserManager().findById(id);
  }
}
