package ua.com.jbs.auction.businesslogic;

import java.sql.Timestamp;
import java.util.List;

import ua.com.jbs.auction.entity.Bid;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;
import ua.com.jbs.auction.entitymanager.BidManager;
import ua.com.jbs.auction.entitymanager.LotManager;

/**
 *
 *
 * @author (astelit.ukr).
 */
public class LotOperations {
  
  public List<Lot> getLots(){
    return new LotManager().getLots();
  }
  
  public Lot createLot(User owner, String name, Timestamp finishDate, float startPrice, String description){
    Lot lot = new Lot(name, finishDate, startPrice, description, Lot.ACTIVE, owner.getId());
    return new LotManager().persist(lot);
  }
  
  public Lot findById(long id){
    return new LotManager().findById(id);
  }
  
  public String getState(Lot lot){
    switch(lot.getState()){
    case 1: return "Active";
    case 2: return "Sold";
    case 3: return "Not sold";
    case 4: return "Cancelled";
    default: return "";
    }
  }
  
  public void updateState(Lot lot, byte state){
    if(lot.getState() == Lot.ACTIVE &&(
        state == Lot.ACTIVE || state == Lot.CANCELLED || state == Lot.NOT_SOLD || state == Lot.SOLD)){
      lot.setState(state);
      new LotManager().update(lot);
    }
  }
  
  public float getLotPrice(Lot lot){
    Bid bid = new BidManager().getMaxBid(lot.getId());
    if(bid == null){
      return lot.getStartPrice();
    }
    return bid.getMoney();
  }
  
  public void cancelTrades(Lot lot, User user){
    if(lot.getOwnerId() == user.getId() && lot.getState() == Lot.ACTIVE){
      lot.setState(Lot.CANCELLED);
      new LotManager().update(lot);
    }
  }
}