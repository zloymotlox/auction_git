package ua.com.jbs.auction.businesslogic;


import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ua.com.jbs.auction.entity.Lot;

/**
 *
 *
 * @author (astelit.ukr).
 */
public class LotCanceller implements Job{

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    JobDataMap data = context.getJobDetail().getJobDataMap();
    long lotId = data.getLong("LotId");
    
    LotOperations operations = new LotOperations();
    Lot lot = operations.findById(lotId);
    if(lot.getState() == Lot.ACTIVE){
      byte state = Lot.ACTIVE;
      if(new BidOperations().getBids(lot).isEmpty()){
        state = Lot.NOT_SOLD;
      }else{
        state =  Lot.SOLD;
      }
      operations.updateState(lot, state);
    }
    Logger logger = Logger.getLogger("Auction");
    logger.info("Lot # " + lot.getId() + " was updated!");
  }
}
