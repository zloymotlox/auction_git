package ua.com.jbs.auction.businesslogic;

import java.util.List;

import ua.com.jbs.auction.entity.Bid;
import ua.com.jbs.auction.entity.Lot;
import ua.com.jbs.auction.entity.User;
import ua.com.jbs.auction.entitymanager.BidManager;
import ua.com.jbs.auction.entitymanager.LotManager;

/**
 *
 *
 * @author (astelit.ukr).
 */
public class BidOperations {
  public List<Bid> getBids(Lot lot){
    return new BidManager().findByLotId(lot.getId());
  }
  
  public Bid createBid(User user, Lot lot, float money){
    if(lot.getState() != Lot.ACTIVE){
      return null;
    }
    
    if(lot.getOwnerId() == user.getId()){
      return null;
    }
    
    if(money <= new LotOperations().getLotPrice(lot)){
      return null;
    }
    
    Bid bid = new Bid(money, lot.getId(), user.getId());
    new BidManager().persist(bid);
    return bid;
  }
}