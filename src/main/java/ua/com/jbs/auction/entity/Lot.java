package ua.com.jbs.auction.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
public class Lot implements Serializable {
  public static final byte ACTIVE = 1;
  public static final byte SOLD = 2;
  public static final byte NOT_SOLD = 3;
  public static final byte CANCELLED = 4;
  
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue
  private long id;

  private String name;
  private Timestamp finishDate;
  private float startPrice;
  private String description;
  private byte state;
  private long ownerId;

  public Lot() {}

  public Lot(String name,
             Timestamp finishDate,
             float startPrice,
             String description,
             byte state,
             long ownerId) {
    this.name = name;
    this.finishDate = finishDate;
    this.startPrice = startPrice;
    this.description = description;
    this.state = state;
    this.ownerId = ownerId;
  }

  public long getId() {
    return id;
  }
  
  public String getName(){
    return name;
  }

  public Timestamp getFinishDate() {
    return finishDate;
  }

  public float getStartPrice() {
    return startPrice;
  }

  public String getDescription() {
    return description;
  }

  public byte getState() {
    return state;
  }
  
  public long getOwnerId() {
    return ownerId;
  }
  
  public void setId(long id){
    this.id = id;
  }
  
  public void setName(String name){
    this.name = name;
  }

  public void setFinishDate(Timestamp finishDate) {
    this.finishDate = finishDate;
  }
  
  public void setStartPrice(float startPrice) {
    this.startPrice = startPrice;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setState(byte state) {
    this.state = state;
  }
  
  public void setOwnerId(long ownerId){
    this.ownerId = ownerId;
  }
}