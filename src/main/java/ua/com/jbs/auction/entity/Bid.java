package ua.com.jbs.auction.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

@Entity
public class Bid implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue
  private long id;

  private float money;
  @Column(nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  public Timestamp date = new Timestamp(new Date().getTime());
  private long lotId;
  private long bidderId;

  public Bid() {}

  public Bid(float money, long lotId, long bidderId) {
    this.money = money;
    this.bidderId = bidderId;
    this.lotId = lotId;
  }

  public long getId() {
    return id;
  }
  
  public float getMoney (){
    return money;
  }

  public Timestamp getDate(){
    return date;
  }
    
  public long getLotId() {
    return lotId;
  }
  
  public long getBidderId() {
    return bidderId;
  }
  
  public void setMoney(float money){
    this.money = money;
  }
  
  public void setLotId(long lotId){
    this.lotId = lotId;
  }
  
  public void setBidderId(long bidderId){
    this.bidderId = bidderId;
  }
}