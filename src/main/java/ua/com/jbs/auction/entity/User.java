package ua.com.jbs.auction.entity;
 
import java.io.Serializable;
import javax.persistence.*;
 
@Entity
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
 
    @Id 
    @GeneratedValue
    private long id;
 
    private String login;
    private String password;
    private String firstName;
    private String lastName;
 
    public User() {
    }
 
    public User(String login, String password, String firstName, String lastName) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    public long getId(){
      return id;
    }
    
    public String getLogin(){
      return login;
    }
    
    public String getPassword(){
      return password;
    }
    
    public String getFirstName(){
      return firstName;
    }
    
    public String getLastName(){
      return lastName;
    }
    
    public void setLogin(String login){
      this.login = login;
    }
    
    public void setPassword(String password){
      this.password = password;
    }
    
    public void setFirstName(String firstName){
      this.firstName = firstName;
    }
    
    public void setLastName(String lastName){
      this.lastName = lastName;
    }
}