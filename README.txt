Requirements: 
1. maven3 (type "sudo apt-get install maven" in terminal) 
2. mysql (type "sudo apt-get install mysql-server mysql-client" in terminal) 
3. existing database: 
-- mysql -uUSER -pPASSWORD (replace USER & PASSWORD to YOUR username & password) 
-- create database DBNAME default character set utf8; (replace DBNAME and exit from mysql) 

Usage: 
--open file persistence.xml (path/to/project/src/main/java/META-INF) 
--set database configuration 
--enter to the project folder(type "cd path/to/project" in terminal) 
-- run project (type "mvn tomcat6:run" in terminal) 

-- type "http://localhost:8080/testtask/" into browser's url line
